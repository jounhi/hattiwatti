var debugLogging = false;
var startDate = '';
var now = new Date();
var endDate = ''+now.toISOString().split('T')[0].replace(/(.+)-(.+)-(.+)/,"$3.$2.$1");
var outputPath = 'out/';
var username = null;
var passwd = null;

function usage() {
  console.log('Usage:\n\tphantomjs hattiwatti.js [Options]\n\tMandatory Options:\n\t\t-sDATE\tStart date dd.mm.yyyy\n\tOptional Options:\n\t\t-eDATE\tEnd date dd.mm.yyyy, (default DATE='+endDate+')\n\t\t-d\tEnable debug log\n\t\t-oPath\tOutput path');
  phantom.exit(0);
}
var args = require('system').args;
if (args.length < 2) {
  usage();
}
else {
    args.forEach(function(arg, i) {
	if(arg.indexOf('-s') == 0) startDate=arg.substring(2);
	else if(arg.indexOf('-e') == 0) endDate=arg.substring(2);
	else if(arg.indexOf('-o') == 0) outputPath=arg.substring(2);
	else if(arg.indexOf('-d') == 0) debugLogging = true;
	else if(arg.indexOf('-u') == 0) username=arg.substring(2);
	else if(arg.indexOf('-p') == 0) passwd=arg.substring(2);
    });
}
re = /^\d{2}\.\d{2}\.\d{4}$/;
if(startDate.length==0 || !startDate.match(re)) {console.log('\nCheck start date format\n',startDate);usage();}
if(endDate.length==0 || !endDate.match(re)) {console.log('\nCheck end date format\n',endDate);usage();}
var fs = null;
if(username == null || passwd == null) {
	fs = require('fs');
	f = fs.open('user.js', 'r');
	content = f.read();
	f.close();
	eval(content);
}

var url = 'https://wattivahti.porienergia.fi/';
var loginPage = 'Login.aspx';
var reportsPage = 'ConsumptionReporting/ConsumptionReport.aspx';
var fileName = getFileName(startDate,endDate);

console.log('Start date: '+startDate);
console.log('End date: '+endDate);
console.log('Output: '+fileName);

function getFileName(start,end) {
    var sParts = start.split('.');
    var eParts = end.split('.');
    var fileName = sParts[2]+sParts[1]+sParts[0]+"_"+eParts[2]+eParts[1]+eParts[0];
    if(outputPath[outputPath.length-1] !== '/') {
        outputPath += '/';
    }
    return outputPath+fileName;
}

phantom.cookiesEnabled = true;
var page = require('webpage').create(),
    testindex = 0,
    loadInProgress = false;
    pagesCount = 0,
    dataMap = [],
    runningSubSteps = false,
    subStepsIndex = -1,
    pagesIndex = 0;
    optionalSubSteps = [];
/*be mercyful for server*/
page.settings.loadImages = false;
page.settings.userAgent = page.settings.userAgent.replace('Unknown','HattiWatti')
console.log('User agent: ' + page.settings.userAgent);

page.onConsoleMessage = function(msg) {
  if(debugLogging)
    console.log(msg);
};

page.onLoadStarted = function() {
  if(debugLogging)
    console.log("page.onLoadStarted");
  loadInProgress = true;
};

page.onLoadFinished = function() {
  loadInProgress = false;
  if(debugLogging) {
    console.log("page.onLoadFinished");
    if(pagesIndex > 0) {
      page.render('table-'+pagesIndex+'.png');
    }
  }
};

page.onResourceReceived = function(response) {
/*
	if(response.stage!="end")return; 
	if(response.contentType == 'application/vnd.ms-excel' || response.contentType == 'image/png') {
		//console.log(response.contentType);
		//console.log(response.url);
		var matches = response.url.match(/[/]([^/]+)$/);
		var fname = 'contents/'+matches[1];
		console.log("Saving "+response.bodySize+" bytes to "+fname);
		fs.write(fname,response.body,'b');
	}
*/
};

page.onResourceError = function(resourceError) {
    console.log('Unable to load resource (#' + resourceError.id + 'URL:' + resourceError.url + ')');
    console.log('Error code: ' + resourceError.errorCode + '. Description: ' + resourceError.errorString);
};

var steps = [
	function() {
		console.log('Open login page');
		page.open(url+loginPage);
	},
	function() {
		console.log('Login');
		page.evaluate(function(username,passwd) {
			document.querySelector('input[id=ContentPlaceHolder1_Login1_Username]').value = username;
			document.querySelector('input[id=ContentPlaceHolder1_Login1_Password]').value = passwd;
			document.querySelector('input[id=ContentPlaceHolder1_Login1_LoginButton]').click();
		},username,passwd);
	},
	function() {
		console.log('Open reports');
		page.evaluate(function() {
			console.log(document.URL);
			console.log(document.title);
		});
		page.open(url+reportsPage);
	}, 
	function() {
		console.log('Open data table');
		page.evaluate(function() {
			console.log(document.URL);
			console.log(document.title);
			document.querySelector('input[id=ContentPlaceHolder1_ActiveIcon2]').click();
		});
	}, 
	function() {
		console.log('Choose period');
		page.evaluate(function() {
			console.log(document.URL);
			console.log(document.title);
			__doPostBack('ctl00$ctl00$ContentPlaceHolder1$ContentPlaceHolderChild1$lbChoosePeriod','');
		});
	}, 
	function() {
		console.log('Set period');
		//page.render('period.png');
		page.evaluate(function(start,end) {
			document.querySelector('input[id=ContentPlaceHolder1_ContentPlaceHolderChild1_tbxBeginDate]').value = start;
			document.querySelector('input[id=ContentPlaceHolder1_ContentPlaceHolderChild1_tbxEndDate]').value = end;
			var elem = document.querySelector('a[id=ContentPlaceHolder1_ContentPlaceHolderChild1_ddlResolution_msa_3]');
			var evt = document.createEvent('MouseEvents');
			evt.initMouseEvent("click", false, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
			elem.dispatchEvent(evt);
		},startDate,endDate);
		subSequentLoadsLeft = false;
	}, 
	function() {
		console.log('Period');
		//page.render(''+fileName+'.png');
	},
	function() {
		pagesCount = page.evaluate(function() {
			return parseInt(jQuery('.dxp-num').last().text());
		});
		console.log('Found data pages: ' + pagesCount);
	},
	function() {
		console.log("Generate new steps");
		var subSteps = [];
		if(pagesCount == null || pagesCount==0) {
			console.log("Read one page");
			subSteps.push(function(){
						console.log('calling read-substep');
						readTableData();
					});
		} else {
			console.log("Read "+pagesCount+" pages");
			for(var i=0;i<pagesCount;i++) {
				var f = { 
					clickfunc : function(){
						   console.log('calling click-substep');
						   openTablePage();
					    },
						readfunc : function(){
						   console.log('calling read-substep');
						   readTableData();
					    }
				};
				subSteps.push(f.clickfunc);
				subSteps.push(f.readfunc);
			}
		}
		if(debugLogging)
			console.log("return generated new steps (should not be executed before this!!!!)");
		return subSteps;
	},
	function() {
		console.log('Done');
	}
];

var cachedPush = Array.prototype.push;

function openTablePage() {
	subStepStarted(subStepsIndex);
  	page.evaluate(function(index) {
		console.log('click timeseries with index='+index);
		aspxGVPagerOnClick('ContentPlaceHolder1_ContentPlaceHolderChild1_gvTimeSeries','PN'+index);
	},pagesIndex);
	subStepDone(subStepsIndex);
	pagesIndex++;
}

function readTableData(){
	var pIndex = subStepsIndex;
	subStepStarted(pIndex);
	var tableData = page.evaluate(function() {
		var index=-1;
		var myData = [];
		jQuery('.dxgv').each(function(i,data){
			/*Timestamp*/
			if(i%3==0){
				/*skip empty tstamp objects after each page change*/
				if($(this).text().length > 0){
					index++;
					var ts = $(this).text().match(/(\d+\.\d+\.\d+)\s(\d+:\d+)/);
					myData.push({
					    "date":ts[1],
					    "time":ts[2]
					});
				}
			}
			/*Energy consumption*/
			if(i%3==1){
				myData[index]["energy"]=$(this).text();
			}
			/*Outside temparature in celsius degrees*/
			if(i%3==2) {
				myData[index]["temperature"]=$(this).text();
			}
		});
		console.log('myData[0]: date time =',myData[0].date, " ", myData[0].time);
		return myData;
	});
	console.log('pageIndex='+pIndex);
	console.log('tableData.length=',tableData.length);
	console.log('tableData[0]: date time =',tableData[0].date, " ", tableData[0].time);
	cachedPush.apply(dataMap,tableData);
	console.log('dataMap.length=',dataMap.length);
	subStepDone(pIndex);
}

function subStepStarted(index){
	console.log("tableLoadStarted");
}
function subStepDone(index){
	console.log("subStepDone");
	runningSubSteps = false;//continue to next substep
}

interval = setInterval(function() {
  if(debugLogging) {
   console.log(loadInProgress);
   console.log(runningSubSteps);
   console.log(steps.length);
   console.log(testindex);
   console.log(subStepsIndex);
   console.log(optionalSubSteps === undefined ? 'empty' : optionalSubSteps.length);
  }
  if (!runningSubSteps && !loadInProgress && typeof steps[testindex] === "function") {
    if(debugLogging)
      console.log('steps left = '+(steps.length-testindex));
    if(optionalSubSteps == undefined || optionalSubSteps.length == 0) {
      //main steps
      optionalSubSteps = steps[testindex]();
      testindex++;
    }
    if(optionalSubSteps != undefined && optionalSubSteps.length != 0){
      //substeps
      runningSubSteps = true;
      subStepsIndex++;
    }
  }
  if(optionalSubSteps != undefined && typeof optionalSubSteps[subStepsIndex]	 !== "function") {
      optionalSubSteps = [];//continue with main steps
      testindex++;
  }
  if(optionalSubSteps != undefined && !loadInProgress && runningSubSteps && typeof optionalSubSteps[subStepsIndex] === "function") {
      optionalSubSteps[subStepsIndex]();    
  }
  if (typeof steps[testindex] !== "function" && (optionalSubSteps == undefined || optionalSubSteps.length == 0)) {
    console.log('Writing json');
    try {
      if(fs == null) {
        fs = require('fs');
      }
      f = fs.open(fileName+'.json', 'w');
      f.write(JSON.stringify(dataMap));
      f.close();
    }
    catch(err) {
      console.log(err.message);
    }
    console.log('Done');
    phantom.exit();
  }
}, 1000);
