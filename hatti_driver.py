#!/usr/bin/env python
'''
deps:

pip install selenium
echo username:yourpassword > user.txt
'''
from selenium import webdriver

class HattiDriver():
    webdriver.DesiredCapabilities.PHANTOMJS['phantomjs.page.customHeaders.User-Agent'] = \
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9) AppleWebKit/537.71 (KHTML, like Gecko) Version/7.0 Safari/537.71'
    # The phantomjs executable is assumed to be in your PATH:
    driver = webdriver.PhantomJS('phantomjs')
    url = 'https://wattivahti.porienergia.fi/'
    loginPage = 'Login.aspx'
    # reportsPage = 'ConsumptionReporting/ConsumptionTable.aspx'
    with open('user.txt','r') as f:
        settings = f.readline().strip().split(':')
    username = settings[0]
    passwd = settings[1]

    def drive(self):
        self.driver.get(self.url+self.loginPage)
        print self.driver.title
        self.driver.find_element_by_id("ctl00_ContentPlaceHolder1_Login1_Username").send_keys(self.username)
        self.driver.find_element_by_id("ctl00_ContentPlaceHolder1_Login1_Password").send_keys(self.passwd)
        self.driver.find_element_by_id("ctl00_ContentPlaceHolder1_Login1_LoginButton").click()
        self.driver.get_screenshot_as_file("./after_login.png")
        '''
        all_cookies = self.driver.get_cookies()
        for cookie_str in all_cookies:
            print "%s" % cookie_str
        '''
        '''
        Do not try navigate too cleverly directly to most interesting pages by accessing url as it breaks the asp.net-thingy
        self.driver.get(self.url+self.reportsPage)
        '''
        self.driver.find_element_by_link_text('Kulutustietoni').click()
        print self.driver.title
        self.driver.find_element_by_id("ctl00_ContentPlaceHolder1_ActiveIcon2").click()
        print self.driver.title 
        self.driver.get_screenshot_as_file("./before_period_link_click.png")
        self.driver.find_element_by_id("ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolderChild1_lbChoosePeriod").click()
        self.driver.get_screenshot_as_file("./after_period_link_click.png")
        print("current_url is now '%s'" % (self.driver.current_url))

    def quit(self):
        self.driver.quit()

if __name__ == '__main__':
    hattiDriver = HattiDriver()
    hattiDriver.drive()
    hattiDriver.quit()