#!/bin/python

import os, json, sqlite3, calendar
from datetime import timedelta, datetime, tzinfo
from subprocess import Popen, PIPE

importPath = '../out'
dbName='energy.db'
tableName='usage'
phantomjs = 'phantomjs'
hattiwatti = os.path.join('..','hattiwatti.js')
defStart = '01.01.2014'

# SQL
tableCreateSql = 'CREATE TABLE if not exists %s(id INTEGER PRIMARY KEY, timestamp INTEGER UNIQUE, energy REAL)' % tableName
insertIntoSql = 'INSERT INTO %s(timestamp,energy) SELECT :timestamp, :energy WHERE NOT EXISTS(SELECT 1 FROM %s where timestamp=:timestamp)' % (tableName, tableName)
lastestDateSql = 'SELECT max(timestamp) FROM %s' % tableName


def createTable(db):
    cursor = db.cursor()
    cursor.execute(tableCreateSql)
    db.commit()

def addToTable(db,data):
    cursor = db.cursor()
    cursor.executemany(insertIntoSql, data)
    db.commit()

def getLatestEntryDate(db):
    cursor = db.cursor()
    cursor.execute(lastestDateSql)
    row = cursor.fetchone()
    db.commit()
    return row[0]

# TZ stuff from https://docs.python.org/2/library/datetime.html
class TZ_FI(tzinfo):
    def utcoffset(self, dt):
        return timedelta(hours=2) + self.dst(dt)
    def dst(self, dt):
        # DST starts last Sunday in March
        d = datetime(dt.year, 4, 1)   # ends last Sunday in October
        self.dston = d - timedelta(days=d.weekday() + 1)
        d = datetime(dt.year, 11, 1)
        self.dstoff = d - timedelta(days=d.weekday() + 1)
        if self.dston <=  dt.replace(tzinfo=None) < self.dstoff:
            return timedelta(hours=1)
        else:
            return timedelta(0)
    def tzname(self,dt):
         return "GMT +2"
class TZ_UTC(tzinfo):
    def utcoffset(self, dt):
        return timedelta(0) + self.dst(dt)
    def dst(self, dt):
        return timedelta(0)
    def tzname(self,dt):
        return "GMT +0"

def runHattiwatti(start, end):
    cwd = os.getcwd()
    os.chdir(os.path.abspath(os.path.dirname(hattiwatti))) 
    p = Popen([phantomjs, os.path.basename(hattiwatti), '-s' + start, '-e' + end], stdout=PIPE, stderr=PIPE)
    output = p.communicate()
    #print output
    os.chdir(cwd) 

def scrape(db):
    timestamp = getLatestEntryDate(db)
    localDate = None
    if timestamp is not None:
        utcDate = datetime.fromtimestamp(timestamp).replace(tzinfo=TZ_UTC())
        localDate = utcDate.astimezone(TZ_FI())

    startDate = localDate.strftime('%d.%m.%Y') if localDate is not None else defStart
    endDate = datetime.today().strftime('%d.%m.%Y')

    print 'Start scraping from: ' + startDate
    print 'End scraping to: ' + endDate

    if startDate == endDate:
        print 'Scraping for nothing...'
        return

    runHattiwatti(startDate, endDate)

    for file in os.listdir(importPath):
        if file.endswith(".json"):
            json_data=open(os.path.join(importPath,file))
            data = json.load(json_data)
            parsed = []
            for d in data:
                item = {}
                item['energy'] = d['energy']
                item['temperature'] = d['temperature']
                date = d['date'].split('.')
                time = d['time'].split(':')
                dt = datetime(int(date[2]), int(date[1]), int(date[0]), int(time[0]), int(time[1]), tzinfo=TZ_FI())
                utcDate = dt.astimezone(TZ_UTC())
                item['timestamp'] = utcDate.strftime('%s')
                parsed.append(item)
            json_data.close()
            addToTable(db,parsed)


db = sqlite3.connect(dbName)
createTable(db)
scrape(db)
db.close()
